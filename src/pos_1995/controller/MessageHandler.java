/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pos_1995.controller;

import pos_1995.model.Message;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Filip Kania
 */
public class MessageHandler {

    
    private static final byte START = (byte) 0x01;
    private static final byte END = (byte) 0x04;

    private String data;
    private byte command;
    private Message decoded_message;
    private byte[] message_to_send;
    private byte[] mode;
   
       
    public MessageHandler () {
        
        mode = new byte[8];
    }
    
    public byte[] PrepareMessage(Message message) {
        
        message_to_send = new byte[3+message.getData().length()];
        
        message_to_send[0] = START;
        
        message_to_send[1] = message.getCommand();
         
        int i;
        
        for (i = 0; i < message.getData().length(); i++) {
            
            message_to_send[i+2] = (byte) message.getData().charAt(i);
        }
                
        message_to_send[i+2] = END;
            
        return message_to_send;
    }
    
    public Message DecodeMessage(byte[] message) throws UnsupportedEncodingException {
        
        for (int i = 0; i <8; i++) {
            mode[i] = 0;
        }
        
        command = 0;
        
        int i = 0;
        int x = 0;
        
        if (message != null) {
            
            do {
                i++;
            } while(message[i-1] != (byte) 0x01);        
            
            if(message[i] != 0) {
                
                command = message [i];
                
            while (message[i] != (byte) 0x04) {
                
                i++;
                mode[x] =  message[i];
                x++;
            }
            }
            
            data = new String(mode, "UTF-8");
            
            decoded_message = new Message(command, data.trim());
            return decoded_message;
            
        } else {
            
            return null;
        }
    }
}
