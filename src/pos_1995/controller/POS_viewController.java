/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pos_1995.controller;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import kea.zmirc.systemintegration.p1.shared.DatagramReader;
import kea.zmirc.systemintegration.p1.shared.UDPSocket;
import kea.zmirc.systemintegration.p1.shared.UDPSocketReceiveHandler;
import kea.zmirc.systemintegration.p1.shared.util.User;
import pos_1995.model.Message;
import pos_1995.util.Regex;

/**
 * FXML Controller class
 *
 * @author FILIP
 */
public class POS_viewController implements Initializable {
    @FXML
    private TextField current_volume;
    @FXML
    private TextField current_price;
    @FXML
    private Label new_price_wrong_format;
    @FXML
    private TextField total_volume;
    @FXML
    private RadioButton idle_state;
    @FXML
    private RadioButton calling_state;
    @FXML
    private RadioButton noozle_on_hook_state;
    @FXML
    private RadioButton fueling_state;
    @FXML
    private Button max_liter_set;
    @FXML
    private Button transaction_done_button;
    @FXML
    private Button max_price_set;
    @FXML
    private Button max_values_reset;
    @FXML
    private TextField max_liter;
    @FXML
    private TextField max_price;
    @FXML
    private TextField new_price;
    @FXML
    private Button new_price_set;
    @FXML
    private RadioButton unavailable_radiobutton;
    @FXML
    private ToggleButton lock_unlock_dispenser;
    @FXML
    private ToggleButton night_toggle;
    @FXML
    private ToggleButton day_toggle;
    @FXML
    private Label current_price_per_liter_label;
    
    private double current_price_per_liter = 11.75;
    
    private double total_volume_ever = 0.0;
    
    private UDPSocket s;
    
    private MessageHandler mh;
    
    private double current_volume_double;
    
    private Message message;
    private Message message_to_send;
    private Message request_status;
    
    private final DecimalFormat price_df = new DecimalFormat("######.##");
    
    private Timeline ask_for_update_timeline;
    private KeyFrame ask_for__update_keyframe;
    
    //Command bytes
    private static final byte change_mode = (byte) 0x41;
    private static final byte change_lock = (byte) 0x42;
    private static final byte set_price_per_liter = (byte) 0x46;
    private static final byte get_status = (byte) 0x47;
    private static final byte current_volume_byte = (byte) 0x48;
    private static final byte current_state = (byte) 0x49;
    private static final byte start_fueling = (byte) 0x53;
    

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        ToggleGroup state_radio_buttons = new ToggleGroup();
        ToggleGroup day_night_buttons = new ToggleGroup();
                
        state_radio_buttons.getToggles().addAll(calling_state,fueling_state, idle_state,noozle_on_hook_state, unavailable_radiobutton);
        day_night_buttons.getToggles().addAll(day_toggle, night_toggle);
        
        current_price_per_liter_label.setText(current_price_per_liter+"");
        
        mh = new MessageHandler();
        
        request_status = new Message(get_status, "");
        message_to_send = new Message();
        
        @SuppressWarnings("unchecked")          
        UDPSocket s = new UDPSocket(User.Kania_Filip_Wojciech, new UDPSocketReceiveHandler() {

        public void onReceive(DatagramReader dr, UDPSocket udps) {
            System.out.println(dr.getPv());
//            System.out.println(dr.getSource());
//            System.out.println(dr.getSourceAddress().toString());
//            System.out.println(dr.getSourcePort());
//            System.out.println(dr.getDestination());
            System.out.println(Arrays.toString(dr.getData()));
            byte[] data = dr.getData();
            
            try {
                message = mh.DecodeMessage(data);
                
                ReactToMessage(message.getCommand(), message.getData());
                
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(POS_viewController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }, 10_001, UDPSocket.DEFAULT_BUFFER_SIZE, true);
        
        this.s = s;
        
        
        RequestUpdates();
    }    

    @FXML
    private void SetMaxLiter(ActionEvent event) {
        //TODO
    }

    @FXML
    private void SetMaxPrice(ActionEvent event) {
        //TODO
    }
    @FXML
    
    private void TransactionDone(ActionEvent event) {
        
        current_price.setText("");
        current_volume.setText("");
        max_price.setText("");
        
        total_volume_ever += current_volume_double;
        
        total_volume.setText(price_df.format(total_volume_ever));
        
        current_volume_double = 0.0;
        
        try {
            message_to_send.setCommand(change_lock);
            message_to_send.setData("UNL");
            
            s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                mh.PrepareMessage(message_to_send));
        } catch (UnknownHostException ex) {
            Logger.getLogger(POS_viewController.class.getName()).log(Level.SEVERE, null, ex);
          }
    }

    @FXML
    private void ResetMaxPriceLiter(ActionEvent event) {
        
        max_liter.setText("");
        max_price.setText("");
        
        //Send info to DIS
    }

    @FXML
    private void SetNewPrice(ActionEvent event) throws UnknownHostException {
        
        if(noozle_on_hook_state.isSelected() || fueling_state.isSelected()) {
            new_price_wrong_format.setTextFill(Color.RED);
            new_price_wrong_format.setText("Finish transaction");
         
        } else if(lock_unlock_dispenser.isSelected()) {
            
                if( new_price.getText().isEmpty() != true) {
            
                    if(Regex.matchPrice(new_price.getText()) != false) {

                        s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002, 
                        mh.PrepareMessage(new Message(set_price_per_liter, new_price.getText())));
                        
                        current_price_per_liter = Double.parseDouble(new_price.getText().replace(",", "."));
                        
                        current_price_per_liter_label.setText(current_price_per_liter+"");
                        new_price.setText("");
                        
                        if(new_price_wrong_format.getText() != null) {
                            
                            new_price_wrong_format.setText(null);
                        }
                    } else {
                        
                        new_price_wrong_format.setTextFill(Color.RED);
                        new_price_wrong_format.setText("Format: ##.##");
                    }
                }
        
        } else {
            new_price_wrong_format.setTextFill(Color.RED);
            new_price_wrong_format.setText("Lock dispenser.");
        }

    }

    @FXML
    private void LockUnlockDispenser(ActionEvent event) throws UnknownHostException {
        
        if(calling_state.isSelected()) {
            
            message_to_send.setCommand(start_fueling);
            message_to_send.setData(max_price.getText());
            
            s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                     mh.PrepareMessage(message_to_send));            
            
        }       
        else if(lock_unlock_dispenser.isSelected()) {
           
            message_to_send.setData("LOC");
            message_to_send.setCommand(change_lock);
                
            s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                     mh.PrepareMessage(message_to_send));      
      
        } else {
           
            try {   

                message_to_send.setData("UNL");
                message_to_send.setCommand(change_lock);
                
                s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                        mh.PrepareMessage(message_to_send));

            } catch (UnknownHostException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
            }     // TODO add your handling code here:
      
        }
    }
    
    public void ReactToMessage(byte command, String data) {
        switch (command) {
                    
                    case current_state : { //ONE OF 5
                        System.out.println("CURRENT STATE");
                        SetCurrentState(data);
                    } break;
                        
                    case current_volume_byte : { //WHILE FUELING
                        
                        SetCurrentVolume(data);
                    } break;
                        
                    default : {
                        System.out.println("default shit");
                    } break;
                }
            
        
    }
    
    public void SetCurrentVolume (String data) {
        //DIS->POS
        current_volume_double = Double.parseDouble(data.replace(",", "."));
        Platform.runLater(new Runnable() {

            @Override
            public void run() {
                current_volume.setText(current_volume_double+"");
                current_price.setText(price_df.format(current_price_per_liter*current_volume_double));
            }
        });
        
        //TODO show current volume
    }
    
    public void SetCurrentState (String data) {
       //DIS->POS
        switch (data) {
            case "IDLE" :
                //TODO
                SetStateRadioButton(idle_state);
                break;
            case "UNAV" : //Unavailable
                //TODO
                break;
            case "CALL" : //Calling
                Platform.runLater(new Runnable() {
                    public void run() {                
                        lock_unlock_dispenser.setSelected(true);
                        SetStateRadioButton(calling_state);
                    }});
                break;
            case "FUEL" : //Fueling
                //TODO
                
                SetStateRadioButton(fueling_state);
                break;

            case "ATUP" : //Awaits total updated ?
                //TODO
                SetStateRadioButton(noozle_on_hook_state);               
                
                break;
                
            default :
                
                break;
                    
        }
    }
    private void SetStateRadioButton(RadioButton rb) {
        
        Platform.runLater(new Runnable() {
            public void run() {        
                rb.setSelected(true);
            }
        });   
        
    }    
    
    @FXML
    private void SetNightMode(ActionEvent event) {

        message_to_send.setCommand(change_mode);
        message_to_send.setData("NIG");
        try {
            s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                    mh.PrepareMessage(message_to_send));
        } catch (UnknownHostException ex) {
            Logger.getLogger(POS_viewController.class.getName()).log(Level.SEVERE, null, ex);
        }              
        
        
    }

    @FXML
    private void SetDayMode(ActionEvent event) {
        
        message_to_send.setCommand(change_mode);
        message_to_send.setData("DAY");
        try {
            s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                    mh.PrepareMessage(message_to_send));
        } catch (UnknownHostException ex) {
            Logger.getLogger(POS_viewController.class.getName()).log(Level.SEVERE, null, ex);
        }           
    }
    
    public void RequestUpdates() {
        
        ask_for_update_timeline = new Timeline();
        ask_for_update_timeline.setCycleCount(Animation.INDEFINITE);
        
        ask_for__update_keyframe = new KeyFrame(Duration.millis(250), new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
            
                try {
                    s.send(User.Conrad_Lasse, InetAddress.getByName("127.0.0.1"), 10_002,
                            mh.PrepareMessage(request_status));
                } catch (UnknownHostException ex) {
                    Logger.getLogger(POS_viewController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        ask_for_update_timeline.getKeyFrames().add(ask_for__update_keyframe);
        ask_for_update_timeline.play();
        
    }
}
