/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pos_1995.model;

/**
 *
 * @author Filip Kania
 */

public class Message {
    
private byte command;
private String data;

    public Message(byte command, String data) {
        
        this.command = command;
        this.data = data;
    }
    
    public Message() {
        
    }
    
    public byte getCommand() {
        return command;
    }
    
    public String getData() {
        return data;
    }
    
    public void setCommand(byte command) {
        this.command = command;
    }
    
    public void setData(String data) {
        this.data = data;
    }
}
